
use v5.24;
use Test2::V0;
use Test::TempDir::Tiny;

ok require App::SensorTag::Plugin::CSVFile, 'module loaded';

like(
    dies { App::SensorTag::Plugin::CSVFile->new },
    qr/Missing required arguments: logdir/,
    'missing required arguments'
);

my $logdir = tempdir;

ok my $plug = App::SensorTag::Plugin::CSVFile->new(logdir => $logdir), 'instantiated with string logdir';
isa_ok $plug, 'App::SensorTag::Plugin::CSVFile';
DOES_ok $plug, 'App::SensorTag::Role::Output';

can_ok $plug, 'logdir';
isa_ok $plug->logdir, 'Path::Tiny';
ok $plug->logdir->is_dir, 'logdir is a dir';

can_ok $plug, '_logfile';
isa_ok $plug->_logfile, 'Path::Tiny';
is $plug->_logfile->parent->stringify, $plug->logdir->stringify, 'logfile is child of logdir';

ok !$plug->_logfile->is_file, '_logfile does not exist yet';

can_ok $plug, '_fh';
ref_ok $plug->_fh, 'GLOB';

ok $plug->_logfile->is_file, '_logfile is a file';

can_ok $plug, 'write';
my $t = rand 100;
ok lives { $plug->write('FAKETAGID', 'temperature', $t)}, 'hopefully wrote a line';

ok $plug->_clear_fh, 'close the file so we can read from it';

my $line = $plug->_logfile->slurp;
like $line, qr/^.+,FAKETAGID,temperature,\d+\.\d+$/, 'line looks good';

done_testing;
