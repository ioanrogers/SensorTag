
use v5.24;
use Test2::V0;
use Test::TempDir::Tiny;

ok require App::SensorTag::Plugin::MQTT, 'module loaded';

like(
    dies { App::SensorTag::Plugin::MQTT->new },
    qr/Missing required arguments: server, tls_crt, tls_key/,
    'missing required arguments'
);

my $tmpdir = tempdir;

ok my $plug = App::SensorTag::Plugin::MQTT->new(server => 'nosuchhost.example.com', tls_crt => "$tmpdir/crt", tls_key => "$tmpdir/key"), 'instantiated with string logdir';
isa_ok $plug, 'App::SensorTag::Plugin::MQTT';
DOES_ok $plug, 'App::SensorTag::Role::Output';
can_ok $plug, 'server';
is $plug->server, 'nosuchhost.example.com';

done_testing;
