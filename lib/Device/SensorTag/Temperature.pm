package Device::SensorTag::Temperature;

# ABSTRACT: IR Temperature Sensor

use v5.20;
use strictures 2;
use Moo;
use namespace::clean;
use experimental 'signatures';

with 'MooX::Log::Any';

has _sensortag => (
    is       => 'ro',
    required => 1,
    weak_ref => 1,
);

has _data_path => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        $_[0]->_sensortag->_dbus_object_path . '/service0022/char0023';
    },
);

has _data_object => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        $_[0]->_sensortag->_bluez_service->get_object($_[0]->_data_path);

    },
);

# has notification => (
#     is => 'rw',
# );

has _configuration_path => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        $_[0]->_sensortag->_dbus_object_path . '/service0022/char0026';
    },
);

has _configuration_object => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        $_[0]
          ->_sensortag->_bluez_service->get_object($_[0]->_configuration_path);

    },
);
#
# has period => (
#     is => 'rw',
# );

sub _bytes_to_celsius ($byte_array) {
    my ($ir_lsb, $ir_msb, $amb_lsb, $amb_msb) = @$byte_array;
    my $raw_16bit_int = unpack 'v', pack 'C2', $amb_lsb,
      $amb_msb;    # little endian
    return {ambient => ($raw_16bit_int / 128)};
}

sub enable ($self) {
    return if $self->_configuration_object->ReadValue(undef)->[0];

    $self->log->debug('Enabling temperature sensor');
    $self->_configuration_object->WriteValue([0x01], undef);
    die 'Failed to enable temperature sensor'
      unless $self->_configuration_object->ReadValue(undef)->[0];
    return;
}

sub read ($self) {
    return _bytes_to_celsius($self->_data_object->ReadValue(undef));
}

# my $temp_period = $bluez_service->get_object("$st_path/service0022/char0028");
# $temp_period->WriteValue([0xFF], undef);

1;

=head1 REFERENCE

http://processors.wiki.ti.com/index.php/CC2650_SensorTag_User's_Guide#IR_Temperature_Sensor

  Type 	         UUID 	Access 	Size (bytes) 	Description
  Data 	         AA01* 	R/N 	4 	            Object[0:7], Object[8:15], Ambience[0:7], Ambience[8:15]
  Notification   2902 	R/W 	2 	            Write 0x0001 to enable notifications, 0x0000 to disable.
  Configuration  AA02* 	R/W 	1 	            Write 0x01 to enable data collection, 0x00 to disable.
  Period 	     AA03* 	R/W 	1 	            Resolution 10 ms. Range 300 ms (0x1E) to 2.55 sec (0xFF). Default 1 second (0x64)
