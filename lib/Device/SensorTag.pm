package Device::SensorTag;

# ABSTRACT: A SensorTag

use v5.24;
use strictures 2;
use utf8;
use Device::SensorTag::Temperature;
use Net::DBus 1.1.0;
use Net::DBus::Reactor;
use Time::Moment;
use Moo;
use namespace::clean;
use experimental 'signatures';

with 'MooX::Log::Any';

has address => (
    is       => 'ro',
    required => 1,
);

has model => (
    is   => 'ro',
    lazy => 1,
);

has firmware_version => (
    is   => 'ro',
    lazy => 1,
);

has _dbus_object_path => (
    is      => 'ro',
    lazy    => 1,
    default => sub($self) {
        my $st_path = '/org/bluez/hci0/dev_' . $self->address;
        $st_path =~ s/:/_/g;
        return $st_path;
    });

has _dbus_object => (
    is      => 'ro',
    lazy    => 1,
    default => sub($self) {
        $self->_bluez_service->get_object($self->_dbus_object_path);
    });

has _bluez_service => (
    is       => 'ro',
    required => 1,
    weak_ref => 1,
);

has temperature => (
    is      => 'ro',
    default => sub {
        return Device::SensorTag::Temperature->new(_sensortag => $_[0]);
    },
);

sub DEMOLISH ($self, $in_global_destruction) {
    if ($self->connected) {
        $self->log->infof('Disconnecting from [%s]...', $self->address);
        $self->_dbus_object->Disconnect;
    }
    return;
}

sub connected ($self) {
    my $props = $self->_dbus_object->GetAll('org.bluez.Device1');
    return $props->{Connected};
}

sub _resolve_services {
    return $_[0]->_dbus_object->Get('org.bluez.Device1', 'UUIDs');
}

sub connect ($self) {
    if (!$self->connected) {
        $self->log->infof('Connecting to [%s]...', $self->address);
        $self->_dbus_object->Connect;

        # check again
        # TODO wait a little, or use event
        if (!$self->connected) {
            die 'Failed to connect';
        }
    }

    $self->log->infof('Connected');

    # GetAll doesn't actually get all, we need to resolve services
    return $self->_resolve_services;
}

# {'Adapter': '/org/bluez/hci0',
#  'Address': 'B0:B4:48:BE:A0:86',
#  'Alias': 'CC2650 SensorTag',
#  'Blocked': False,
#  'Connected': True,
#  'LegacyPairing': False,
#  'Modalias': 'bluetooth:v000Dp0000d0110',
#  'Name': 'CC2650 SensorTag',
#  'Paired': False,
#  'ServicesResolved': True,
#  'Trusted': False,
#  'UUIDs': ['00001800-0000-1000-8000-00805f9b34fb',
#            '00001801-0000-1000-8000-00805f9b34fb',
#            '0000180a-0000-1000-8000-00805f9b34fb',
#            '0000180f-0000-1000-8000-00805f9b34fb',
#            '0000ffe0-0000-1000-8000-00805f9b34fb',
#            'f000aa00-0451-4000-b000-000000000000',
#            'f000aa20-0451-4000-b000-000000000000',
#            'f000aa40-0451-4000-b000-000000000000',
#            'f000aa64-0451-4000-b000-000000000000',
#            'f000aa70-0451-4000-b000-000000000000',
#            'f000aa80-0451-4000-b000-000000000000',
#            'f000ac00-0451-4000-b000-000000000000',
#            'f000ccc0-0451-4000-b000-000000000000',
#            'f000ffc0-0451-4000-b000-000000000000']}

# sub temp_props_changed_cb($interface, $property, $unknown) {
#     unless ($interface eq 'org.bluez.GattCharacteristic1') {
#         say "Got unexpected interface: $interface";
#         return;
#     }
#     unless (exists $property->{Value}) {
#         say "unexpected propery change! " . join(', ', keys %$property);
#         return;
#     }
#
#     return log_temp($property->{Value});
# }

# $temp->connect_to_signal('PropertiesChanged', \&temp_props_changed_cb);

1;

=head1 REFERENCE

http://processors.wiki.ti.com/index.php/CC2650_SensorTag_User's_Guide
