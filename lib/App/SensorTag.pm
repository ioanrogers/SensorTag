package App::SensorTag;

# ABSTRACT: guts of sensortag.pl

use v5.24;
use strictures 2;
use sigtrap
  qw/handler _signal_handler normal-signals stack-trace error-signals/;
use utf8;
use Device::SensorTag;
use Log::Any::Adapter;
use Moo;
use namespace::clean;
use CLI::Osprey;
use experimental 'signatures';

with 'MooX::Log::Any';

option sensortag => (
    is         => 'ro',
    format     => 's',
    repeatable => 1,
    doc        => 'The bluetooth address of a SensorTag to use. Repeatable',
);

has _sensortag => (
    is      => 'ro',
    default => sub { {} },
);

has _dbus_reactor => (
    is      => 'ro',
    lazy    => 1,
    default => sub { Net::DBus::Reactor->main });

has _dbus => (
    is      => 'ro',
    lazy    => 1,
    default => sub { Net::DBus->system });

has _bluez_service => (
    is      => 'ro',
    lazy    => 1,
    default => sub { $_[0]->_dbus->get_service('org.bluez') });

has _outputs => (
    is      => 'ro',
    default => sub { [] },
);

sub DEMOLISH ($self, $in_global_destruction) {
    $self->_dbus_reactor->shutdown;
    return;
}

sub _signal_handler ($signal) {
    say "Got SIG$signal";
    exit;
}

sub _log_temp ($self, $tag) {

    my $temp = $self->_sensortag->{$tag}->{device}->temperature->read;

    for my $output ($self->_outputs->@*) {
        $output->write($self->_sensortag->{$tag}->{device}->address,
            'temperature', $temp->{ambient});
    }

    $self->log->infof(
        'SensorTag %s, %.4f℃',
        $self->_sensortag->{$tag}->{device}->address,
        $temp->{ambient});

    return;
}

sub run ($self) {

    Log::Any::Adapter->set(
        'Screen',
        min_level => 'debug',
        stderr    => 0,

        # formatter => sub { "LOG: $_[1]" }, # default none
    );

    say $ENV{MQTT_SERVER};
    say $ENV{MQTT_TLS_CRT};
    say $ENV{MQTT_TLS_KEY};

    require App::SensorTag::Plugin::MQTT;
    push @{$self->_outputs},
      App::SensorTag::Plugin::MQTT->new(
        server  => $ENV{MQTT_SERVER},
        tls_crt => $ENV{MQTT_TLS_CRT},
        tls_key => $ENV{MQTT_TLS_KEY},
      );

    require App::SensorTag::Plugin::CSVFile;
    push @{$self->_outputs},
      App::SensorTag::Plugin::CSVFile->new(logdir => 'readings',);

    # setup sensortags
    for my $tag_addr (@{$self->sensortag}) {

        # TODO give tags names
        my $device = Device::SensorTag->new(
            address        => $tag_addr,
            _bluez_service => $self->_bluez_service,
        );

        $device->connect;
        $device->temperature->enable;

        $self->_sensortag->{$tag_addr} = {device => $device,};

        # $self->_log_temp($tag_addr);
        $self->_dbus_reactor->add_timeout(60_000,
            sub { $self->_log_temp($tag_addr) });
    }

    $self->log->info('Watching sensors');
    $self->_dbus_reactor->run;
}

1;
