package App::SensorTag::Plugin::CSVFile;

# ABSTRACT: send SensorTag readings to a CSV file

use v5.24;
use strictures 2;
use Specio::Library::Path::Tiny;
use Time::Moment;
use Moo;
use namespace::clean;
use experimental 'signatures';

with qw/App::SensorTag::Role::Output MooX::Log::Any/;

=attr C<logdir>

Path to where the directory to which sensor readings will be written

=cut

has logdir => (
    is       => 'ro',
    isa      => t('AbsPath'),
    required => 1,
    coerce   => t('AbsPath')->coercion_sub,
    trigger  => sub {
        $_[0]->logdir->mkpath unless $_[0]->logdir->is_dir;
    },

);

has _logfile => (
    is  => 'lazy',
    isa => t('AbsPath'),
);

has _fh => (
    is      => 'lazy',
    clearer => 1,
);

sub _build__logfile ($self) {
    my $file = $_[0]->logdir->child('sensorlog.csv');
    return $file;
}

sub _build__fh ($self) {
    my $fh = $self->_logfile->opena({locked => 1});
    $fh->autoflush;
    return $fh;
}

sub write ($self, $tag_id, $type, $msg) {
    $self->_fh->printf("%s,%s,%s,%f\n", Time::Moment->now_utc, $tag_id, $type,
        $msg);
    return;
}

1;
