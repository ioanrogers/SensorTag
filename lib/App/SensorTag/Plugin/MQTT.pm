package App::SensorTag::Plugin::MQTT;

# ABSTRACT: send SensorTag readings to an MQTT broker

use v5.24;
use strictures 2;
use Net::MQTT::Simple::SSL;
use Moo;
use namespace::clean;
use experimental 'signatures';

with qw/App::SensorTag::Role::Output MooX::Log::Any/;

has server => (
    is       => 'ro',
    required => 1,
);

has tls_key => (
    is       => 'ro',
    required => 1,
);

has tls_crt => (
    is       => 'ro',
    required => 1,
);

has tls_ca_crt => (is => 'ro',);

has topic_base => (
    is      => 'ro',
    default => 'sensors',
);

has _mqtt => (is => 'lazy',);

sub _build__mqtt ($self) {
    my $opt = {
        SSL_cert_file => $self->tls_crt,
        SSL_key_file  => $self->tls_key,
        ,
    };

    $opt->{SSL_ca_file} = $self->tls_ca_crt if $self->tls_ca_crt;

    $self->log->debugf('MQTT: server=%s tls_crt=%s tls_key=%s',
        $self->server, $self->tls_crt, $self->tls_key);
    my $mqtt = Net::MQTT::Simple::SSL->new($self->server, $opt);

    return $mqtt;
}

# tag identifier, sensor type, message
sub write ($self, $tag_id, $type, $msg) {

    my $topic = sprintf '%s/%s/%s', $self->topic_base, $tag_id, $type;

    my $r = $self->_mqtt->retain($topic => $msg);

    $self->log->warn("Failed to send message to $self->server") unless $r;

    return;
}

1;
