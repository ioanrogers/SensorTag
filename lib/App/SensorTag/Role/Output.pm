package App::SensorTag::Role::Output;

# ABSTRACT: Role for plugins which output sensor readings somewhere

use v5.24;
use strictures 2;
use Moo::Role;

requires 'write';

1;
