#!/usr/bin/env perl

# ABSTRACT: Log data from Texas Instruments SensorTags
# PODNAME: sensortag.pl

use v5.24;
use strictures 2;
use utf8;
use App::SensorTag;

binmode STDOUT, ':encoding(UTF-8)';
binmode STDERR, ':encoding(UTF-8)';
STDOUT->autoflush(1);

App::SensorTag->new_with_options->run;
